package org.gcube.common.clients.stubs.jaxws.handlers;

import javax.xml.namespace.QName;

import org.gcube.common.clients.stubs.jaxws.GCoreService;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;

import jakarta.xml.soap.SOAPHeader;
import jakarta.xml.ws.handler.soap.SOAPMessageContext;

public class AuthorizationHandler extends AbstractHandler {

	/** Namespace of scope-related headers */
	public static final String SCOPE_NS = "http://gcube-system.org/namespaces/scope";
	
	/** Name of the scope call header. */
	public static final String SCOPE_HEADER_NAME = "scope";
	public static final QName SCOPE_QNAME = new QName(SCOPE_NS,SCOPE_HEADER_NAME);

	
	@Override
	public void handleRequest(GCoreService<?> target, SOAPHeader header, SOAPMessageContext context) throws Exception {
		Secret secret =  SecretManagerProvider.get();
		if (secret!= null)
			addHeader(header,SCOPE_QNAME, secret.getContext());
	}


}

